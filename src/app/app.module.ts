import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';

import { TranslateLoader, TranslateModule, TranslatePipe, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxElectronModule } from 'ngx-electron';

import { AppRoutingModule } from './app-routing.module';

import { AnalyzeComponent } from './home/analyze/analyze.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SettingsComponent } from './settings/settings.component';
import { SettingChooserComponent } from './settings/setting-chooser/setting-chooser.component';
import { SettingSliderComponent } from './settings/setting-slider/setting-slider.component';
import { ResultsTableComponent } from './home/results-table/results-table.component';

import { AnalyzeService } from './shared/service/analyze.service';
import { AnalyzeSettingsService } from './shared/service/analyze-settings.service';
import { AnalyzeSettingsComponent } from './settings/analyze-settings/analyze-settings.component';
import { SettingsService } from './shared/service/settings.service';
import { ResultsService } from './shared/service/results.service';
import { DetailsComponent } from './details/details.component';
import { PropertyDialogComponent } from './home/analyze/property-dialog/property-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DetailsFooterComponent } from './details/details-footer/details-footer.component';
import { ThresholdDialogComponent } from './details/details-footer/threshold-dialog/threshold-dialog.component';
import { ScanStatusService } from './shared/service/scan-status.service';
import { ScanResultDialogComponent } from './details/details-footer/scan-result-dialog/scan-result-dialog.component';
import { ReversePipe } from './shared/pipes/reverse.pipe';
import { SystemSettingsService } from './shared/service/system-settings.service';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    AnalyzeComponent,
    AnalyzeSettingsComponent,
    HomeComponent,
    SettingsComponent,
    SettingChooserComponent,
    SettingSliderComponent,
    ResultsTableComponent,
    DetailsComponent,
    PropertyDialogComponent,
    DetailsFooterComponent,
    ThresholdDialogComponent,
    ScanResultDialogComponent,
    ReversePipe
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatDialogModule,
    MatTooltipModule,
    NgxElectronModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    AnalyzeService,
    AnalyzeSettingsService,
    ResultsService,
    ScanStatusService,
    SettingsService,
    TranslatePipe,
    TranslateService,
    SystemSettingsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
