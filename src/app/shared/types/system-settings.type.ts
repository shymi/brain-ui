import { Language } from './language.type';

export interface SystemSettings {
  language: string;
  availableLanguages: Array<Language>;
}
