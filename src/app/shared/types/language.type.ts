export interface Language {
  label: string;
  sign: string;
}
