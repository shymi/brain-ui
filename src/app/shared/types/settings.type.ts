export interface Settings {
  dicomDir: string;
  serviceDir: string;
  modelFile: string;
  resultsDir: string;
  threshold: number;
}
