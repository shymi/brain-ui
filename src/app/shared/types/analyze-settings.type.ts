export interface AnalyzeSettings {
  executor: string;
  scriptName: string;
  settingsParamName: string;
  folderParamName: string;
  modelParamName: string;
  thresholdParamName: string;
}
