import { ScanResultMain } from './scan-result-main.type';
import { ScanResultPositive } from './scan-result-positive.type';

export interface ScanResult {
  path: string;
  main: ScanResultMain;
  positive?: ScanResultPositive[];
}
