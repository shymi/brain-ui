export interface ScanStatus {
  result: string;
  message: string;
}
