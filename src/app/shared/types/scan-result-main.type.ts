export interface ScanResultMain {
  total?: number;
  positive?: number;
  negative?: number;
  Patient?: string;
  Date?: string;
  Time?: string;
  Born?: string;
  Sex?: string;
  Age?: string;
  Institution?: string;
  InstitutionAddress?: string;
  'Study Name'?: string;
  'Series Name'?: string;
  'Model Name'?: string;
  Threshold?: string;
}
