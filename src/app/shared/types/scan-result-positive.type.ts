export interface ScanResultPositive {
  global?: number;
  positive?: number;
  negative?: number;
  dcm?: string;
  png?: string;
  any?: any[];
  epidural?: any[];
  intraparenchymal?: any[];
  intraventricular?: any[];
  subarachnoid?: any[];
  subdural?: any[];
}
