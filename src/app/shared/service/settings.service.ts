import { Injectable } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { BehaviorSubject } from 'rxjs';

import { Settings } from '../types/settings.type';

@Injectable()
export class SettingsService {
  defaultSettings: Settings = {
    dicomDir: '',
    serviceDir: '',
    modelFile: '',
    resultsDir: '',
    threshold: 0.45
  };

  settingsSubject = new BehaviorSubject<Settings>(this.defaultSettings);

  constructor(private _electronService: ElectronService) {
    this._electronService.ipcRenderer.invoke('load-settings', this.defaultSettings)
      .then((result) => {
        this.settingsSubject.next(result);
      });
  }

  setSettings(settings: Settings): void {
    this.settingsSubject.next(settings);
    this._electronService.ipcRenderer.invoke('save-settings', settings);
  }
}
