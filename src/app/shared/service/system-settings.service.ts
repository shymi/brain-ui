import { Injectable, OnDestroy } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { BehaviorSubject, Subscription } from 'rxjs';

import { SystemSettings } from '../types/system-settings.type';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class SystemSettingsService implements OnDestroy {
  private _settingsSub: Subscription | null = null;

  defaultSettings: SystemSettings = {
    language: 'bg',
    availableLanguages: [
      {label: 'Български', sign: 'bg'},
      {label: 'English', sign: 'en'}
    ]
  };

  sysSettingsSubject = new BehaviorSubject<SystemSettings>(this.defaultSettings);

  constructor(
    private _electronService: ElectronService,
    private _translateService: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    this._translateService.setDefaultLang('bg');

    this._settingsSub = this.sysSettingsSubject.subscribe(settings => this.updateLang(settings.language));

    this._electronService.ipcRenderer.invoke('load-system-settings', this.defaultSettings)
      .then((result) => {
        this.sysSettingsSubject.next(result);
      });
  }

  setSettings(settings: SystemSettings): void {
    this.sysSettingsSubject.next(settings);
    this._electronService.ipcRenderer.invoke('save-system-settings', settings);
  }

  ngOnDestroy(): void {
    if (this._settingsSub) {
      this._settingsSub.unsubscribe();
    }
  }

  private updateLang(lang: string): void {
    // the lang to use
    this._translateService.use(lang);
  }
}
