import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { ElectronService } from 'ngx-electron';
import { ScanStatus } from '../types/scan-status.type';
import { SettingsService } from './settings.service';

@Injectable()
export class ScanStatusService {
  static DEFAULT_SCAN_STATUS = {result: 'OK', message: ''};

  private _settingsSub: Subscription | null = null;
  private _resultsDir = '';

  lastScanStatus = new BehaviorSubject<ScanStatus>(ScanStatusService.DEFAULT_SCAN_STATUS);

  constructor(
    private _electronService: ElectronService,
    private _settingsService: SettingsService) {
    this._settingsSub = this._settingsService.settingsSubject.subscribe(settings => {
      this._resultsDir = settings.resultsDir;
    });
  }

  loadScanStatus(): null | Promise<any> {
    if (this._resultsDir) {
      return this._electronService.ipcRenderer
        .invoke('get-latest-scan-status', this._resultsDir)
        .then((lastScan) => {
          this.lastScanStatus.next(lastScan);
        });
    }

    return null;
  }
}
