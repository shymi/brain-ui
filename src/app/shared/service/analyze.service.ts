import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ElectronService } from 'ngx-electron';

import { AnalyzeSettings } from '../types/analyze-settings.type';

import { AnalyzeSettingsService } from './analyze-settings.service';
import { SettingsService } from './settings.service';
import { Settings } from '../types/settings.type';
import { ScanStatusService } from './scan-status.service';

@Injectable()
export class AnalyzeService {
  private _settings: Settings | null = null;
  private _analyzeSettings: AnalyzeSettings | null = null;

  analysisInProgressSubj = new BehaviorSubject<boolean>(false);

  constructor(
    private _analyzeSettingsService: AnalyzeSettingsService,
    private _electronService: ElectronService,
    private _scanStatusService: ScanStatusService,
    private _settingsService: SettingsService) {
    this._settingsService.settingsSubject.subscribe(settings => {
      this._settings = settings;
    });

    this._analyzeSettingsService.analyzeSettingsSubject.subscribe(settings => {
      this._analyzeSettings = settings;
    });

    this._electronService.ipcRenderer.on('python-data', (event, data) =>{
      console.log('PYTHON_LOG', data)
    });

    this._electronService.ipcRenderer.on('python-error', (event, data) =>{
      console.log('PYTHON_ERROR', data)
    });

    this._electronService.ipcRenderer.on('python-close', (event, data) => {
      console.log('PYTHON_EXIT_CODE', data);
      this._scanStatusService.loadScanStatus();
      this.analysisInProgressSubj.next(false);
    });
  }

  analyze(): void {
    if (this._settings && this._analyzeSettings) {
      this._electronService.ipcRenderer.invoke(
        'analyze-start',
        this._analyzeSettings,
        this._settings.serviceDir
      );

      this.analysisInProgressSubj.next(true);
      return;
    }

    this.analysisInProgressSubj.next(false);
  }

  analyzeFromDetails(folder: string, model: string, threshold: number): void {
    if (this._settings && this._analyzeSettings) {
      this._electronService.ipcRenderer.invoke(
        'analyze-details-start',
        this._analyzeSettings,
        this._settings.serviceDir,
        folder,
        model,
        threshold
      );

      this.analysisInProgressSubj.next(true);
      return;
    }

    this.analysisInProgressSubj.next(false);
  }
}
