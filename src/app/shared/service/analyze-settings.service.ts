import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ElectronService } from 'ngx-electron';
import { AnalyzeSettings } from '../types/analyze-settings.type';

@Injectable()
export class AnalyzeSettingsService {
  defaultSettings: AnalyzeSettings = {
    executor: 'python3',
    scriptName: 'analyze.py',
    settingsParamName: '-settings',
    thresholdParamName: '-threshold',
    folderParamName: '-folder',
    modelParamName: '-model'
  };

  analyzeSettingsSubject = new BehaviorSubject<AnalyzeSettings>(this.defaultSettings);
  settingsLocationSubject = new BehaviorSubject<string>('./');

  constructor(private _electronService: ElectronService) {
    this._electronService.ipcRenderer.invoke('analyze-load-settings', this.defaultSettings)
      .then((result) => {
        this.analyzeSettingsSubject.next(result);
      });
    this._electronService.ipcRenderer.invoke('settings-get-location')
      .then((result) => {
        this.settingsLocationSubject.next(result);
      });
  }

  setSettings(settings: AnalyzeSettings): void {
    this.analyzeSettingsSubject.next(settings);
    this._electronService.ipcRenderer.invoke('analyze-save-settings', settings);
  }
}
