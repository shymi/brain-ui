import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

import { SettingsService } from './settings.service';

import { ScanResult } from '../types/scan-result.type';

import { ElectronService } from 'ngx-electron';
import { Router } from '@angular/router';

@Injectable()
export class ResultsService {
  private _settingsSub: Subscription | null = null;
  private _resultsDir = '';
  results = new BehaviorSubject<ScanResult[]>([]);

  constructor(
    private _electronService: ElectronService,
    private _router: Router,
    private _settingsService: SettingsService) {
    this._settingsSub = this._settingsService.settingsSubject.subscribe(settings => {
      this._resultsDir = settings.resultsDir;
      this.loadResults();
    });
  }

  loadResults(): null | Promise<any> {
    if (this._resultsDir) {
      return this._electronService.ipcRenderer
        .invoke('load-results', this._resultsDir)
        .then((results) => {
          this.results.next(results);
        });
    }

    return null;
  }

  getSingleResult(index: number): ScanResult | null {
    if (this.results.value.length > 0) {
      return this.results.value[index];
    }

    return null;
  }

  showDetails(index: number): void {
    if (
      this.results.value[index]
      && this.results.value[index].main.positive
      && this.results.value[index].main.positive! > 0) {
      this._router.navigate(['details', index]);
    }
  }
}
