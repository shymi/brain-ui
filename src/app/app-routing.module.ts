import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AnalyzeSettingsComponent } from './settings/analyze-settings/analyze-settings.component';
import { DetailsComponent } from './details/details.component';
import { HomeComponent } from './home/home.component';
import { SettingsComponent } from './settings/settings.component';

const appRoutes = [
  { path: '', component: HomeComponent },
  { path: 'settings', component: SettingsComponent},
  { path: 'settings/analyze', component: AnalyzeSettingsComponent },
  { path: 'details/:id', component: DetailsComponent },
  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
