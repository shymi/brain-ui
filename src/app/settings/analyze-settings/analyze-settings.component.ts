import { Component, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';

import { AnalyzeSettings } from '../../shared/types/analyze-settings.type';
import { AnalyzeSettingsService } from '../../shared/service/analyze-settings.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SettingsService } from '../../shared/service/settings.service';
import { Settings } from '../../shared/types/settings.type';

@Component({
  selector: 'app-analyze-settings',
  templateUrl: './analyze-settings.component.html'
})
export class AnalyzeSettingsComponent implements OnDestroy {
  private _analyzeSettingsSub: Subscription;
  private _settingsSub: Subscription;
  private _settingsLocationSub: Subscription;
  @ViewChild('f') settingsForm?: NgForm;
  analyzeSettings: AnalyzeSettings = {
    executor: '',
    scriptName: '',
    settingsParamName: '',
    folderParamName: '',
    thresholdParamName: '',
    modelParamName: ''
  };
  settings: Settings = {
    dicomDir: '',
    serviceDir: '',
    modelFile: '',
    resultsDir: '',
    threshold: 0
  };

  settingsLocation = './';

  constructor(
    private _analyzeSettingsService: AnalyzeSettingsService,
    private _settingsService: SettingsService,
    private _router: Router) {
    this._analyzeSettingsSub = this._analyzeSettingsService.analyzeSettingsSubject.subscribe(settings => {
      this.analyzeSettings = settings;
    });
    this._settingsSub = this._settingsService.settingsSubject.subscribe(settings => {
      this.settings = settings;
    });
    this._settingsLocationSub = this._analyzeSettingsService.settingsLocationSubject.subscribe(settings => {
      this.settingsLocation = settings;
    });
  }

  onSubmit(): void {
    this._analyzeSettingsService.setSettings(this.settingsForm?.value);
    this._router.navigate(['/']);
  }

  ngOnDestroy(): void {
    if (this._analyzeSettingsSub) {
      this._analyzeSettingsSub.unsubscribe();
    }

    if (this._settingsSub) {
      this._settingsSub.unsubscribe();
    }

    if (this._settingsLocationSub) {
      this._settingsLocationSub.unsubscribe();
    }
  }
}
