import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { SettingsService } from '../shared/service/settings.service';
import { Settings } from '../shared/types/settings.type';
import { Subscription } from 'rxjs';
import { SystemSettingsService } from '../shared/service/system-settings.service';
import { SystemSettings } from '../shared/types/system-settings.type';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnDestroy {
  @ViewChild('f') settingsForm?: NgForm;
  @ViewChild('lang') langDropdown?: ElementRef;
  private _settingsSub: Subscription | null = null;
  private _sysSettingsSub: Subscription | null = null;
  settings: Settings;
  sysSettings: SystemSettings;

  constructor(
    private _settingsService: SettingsService,
    private _sysSettingsService: SystemSettingsService,
    private _route: ActivatedRoute,
    private _router: Router) {
    this.settings = this._settingsService.defaultSettings;
    this.sysSettings = this._sysSettingsService.defaultSettings;

    this._settingsSub = this._settingsService.settingsSubject.subscribe(settings => {
      this.settings = settings;
    });

    this._sysSettingsSub = this._sysSettingsService.sysSettingsSubject.subscribe(settings => {
      this.sysSettings = settings;
    });
  }

  ngOnDestroy(): void {
    if (this._settingsSub) {
      this._settingsSub.unsubscribe();
    }
  }

  onSubmit(): void {
    this._settingsService.setSettings(this.settingsForm?.value);

    if (this.langDropdown) {
      this.sysSettings.language = this.langDropdown.nativeElement.value;
      this._sysSettingsService.setSettings(this.sysSettings);
    }

    this._router.navigate(['/']);
  }
}
