import { Component, forwardRef, Input } from '@angular/core';

import { ElectronService } from "ngx-electron";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { TranslatePipe } from '@ngx-translate/core';

@Component({
  selector: 'app-setting-chooser',
  templateUrl: './setting-chooser.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => SettingChooserComponent),
    }
  ]
})
export class SettingChooserComponent implements ControlValueAccessor {
  @Input() type: 'openDirectory' | 'openFile' = 'openDirectory';
  @Input() buttonName = this._translatePipe.transform('SETTINGS.GENERIC-BTN');
  @Input() buttonLabel = '';
  value: string | null = null;
  onChange: any;
  onTouched: any;

  constructor(
    private _electronService: ElectronService,
    private _translatePipe: TranslatePipe) { }

  browse(): void {
    this._electronService.remote.dialog
      .showOpenDialog({title: this.buttonLabel ?
          this.buttonLabel : this._translatePipe.transform('SETTINGS.GENERIC-DIR-SELECTION'), properties: [this.type]})
      .then(
        folderPath => {
          if (folderPath === undefined){
            return;
          }

          if (folderPath.filePaths[0]) {
            this.value = folderPath.filePaths[0];
            this.onChange(this.value);
          }
        }
      );
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: string): void {
    this.value = value;
  }
}
