import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ScanResult } from '../shared/types/scan-result.type';
import { ResultsService } from '../shared/service/results.service';
import { Subscription } from 'rxjs';
import { ElectronService } from 'ngx-electron';
import { ScanResultPositive } from '../shared/types/scan-result-positive.type';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html'
})
export class DetailsComponent implements OnDestroy, AfterViewInit {
  private _routeParamsSub: Subscription;
  showImage = true;
  currentIndex = 0;
  scanResult: ScanResult | null = {path: '', main: {}};
  @ViewChild('pictureHolder', {static: false}) pictureHolder!: ElementRef;

  constructor(
    private _electronService: ElectronService,
    private _route: ActivatedRoute,
    private _resultsService: ResultsService) {
    this._routeParamsSub = this._route.params.subscribe(
      (params: Params) => {
        this.scanResult = this._resultsService.getSingleResult(params.id);
      }
    );
  }

  ngAfterViewInit(): void {
    if (this.scanResult
      && this.scanResult.positive
      && this.scanResult.positive.length > 0) {
      this.showItem(0);
    }
  }

  ngOnDestroy(): void {
    if (this._routeParamsSub) {
      this._routeParamsSub.unsubscribe();
    }
  }

  showItem(index: number): void {
    if (this.scanResult && this.scanResult.positive) {
      this._electronService.ipcRenderer
        .invoke('build-png-path-result', this.scanResult.path, this.scanResult.positive[index].png)
        .then((result) => {
          this.showImage = true;
          this.pictureHolder.nativeElement.src = result.toString();
        });
      this.currentIndex = index;
    } else {
      this.showImage = false;
    }
  }

  isPositive(property: keyof ScanResultPositive): boolean {
    // @ts-ignore
    return this.scanResult.positive[this.currentIndex][property][0] === '1';
  }

  getDetailsFolder(): string {
    return this.scanResult ? this.scanResult.path : '';
  }
}
