import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-threshold-dialog',
  templateUrl: './threshold-dialog.component.html'
})
export class ThresholdDialogComponent {
  value = 0.45;

  constructor(
    public dialogRef: MatDialogRef<ThresholdDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { threshold: number }) {
    this.value = data.threshold
  }

  close(save: boolean): void {
    if (save) {
      this.dialogRef.close(this.value);
      return;
    }

    this.dialogRef.close(this.data.threshold);
  }
}
