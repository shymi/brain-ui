import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-scan-result-dialog',
  templateUrl: './scan-result-dialog.component.html'
})
export class ScanResultDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<ScanResultDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { message: number }) {
  }

  close(confirm: boolean): void {
    this.dialogRef.close(confirm);
  }
}
