import { Component, Input, NgZone, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { TranslatePipe } from '@ngx-translate/core';
import { ElectronService } from 'ngx-electron';
import { Subscription } from 'rxjs';

import { AnalyzeService } from '../../shared/service/analyze.service';
import { SettingsService } from '../../shared/service/settings.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ThresholdDialogComponent } from './threshold-dialog/threshold-dialog.component';
import { PropertyDialogComponent } from '../../home/analyze/property-dialog/property-dialog.component';
import { Settings } from '../../shared/types/settings.type';
import { ResultsService } from '../../shared/service/results.service';
import { ScanStatusService } from '../../shared/service/scan-status.service';
import { ScanResultDialogComponent } from './scan-result-dialog/scan-result-dialog.component';

@Component({
  selector: 'app-details-footer',
  templateUrl: './details-footer.component.html'
})
export class DetailsFooterComponent implements OnDestroy {
  private _settingsSub: Subscription | null = null;
  private _analysisInProgressSub: Subscription | null = null;
  private _scanStatusSub: Subscription | null = null;
  private _scanStatus = ScanStatusService.DEFAULT_SCAN_STATUS;

  @Input() detailsFolder = '';

  model = '';
  threshold = 0.45;
  settings: Settings;
  analysisInProgress = false;

  constructor(
    private _analyzeService: AnalyzeService,
    private _electronService: ElectronService,
    private _matDialog: MatDialog,
    private _ngZone: NgZone,
    private _resultsService: ResultsService,
    private _router: Router,
    private _scanStatusService: ScanStatusService,
    private _settingsService: SettingsService,
    private _translatePipe: TranslatePipe) {
    this.settings = this._settingsService.defaultSettings;

    this._settingsSub = this._settingsService.settingsSubject.subscribe(settings => {
      this.settings = settings;
      this.model = settings.modelFile;
      this.threshold = settings.threshold;
    });

    this._scanStatusSub = this._scanStatusService.lastScanStatus.subscribe(scanStatus => {
      this._scanStatus = scanStatus;
    });

    this._analysisInProgressSub = this._analyzeService.analysisInProgressSubj.subscribe(isInProgress => {
      const isNewResult = this.analysisInProgress && !isInProgress;
      this.analysisInProgress = isInProgress;

      if (isNewResult) {
        this._ngZone.run(() => {
          const loadResultsPromise = this._resultsService.loadResults();
          const scanStatusPromise = this._scanStatusService.loadScanStatus();

          if (loadResultsPromise && scanStatusPromise) {
            loadResultsPromise.then(() => scanStatusPromise.then(() => this._resolveEndScan()));
            return;
          }

          if (loadResultsPromise) {
            loadResultsPromise.then(() => this._resolveEndScan());
            return;
          }

          if (scanStatusPromise) {
            scanStatusPromise.then(() => this._resolveEndScan());
            return;
          }

          this._resolveEndScan();
        });
      }
    });
  }

  ngOnDestroy(): void {
    if (this._settingsSub) {
      this._settingsSub.unsubscribe();
    }

    if (this._analysisInProgressSub) {
      this._analysisInProgressSub.unsubscribe();
    }

    if (this._scanStatusSub) {
      this._scanStatusSub.unsubscribe();
    }
  }

  navigateToHome(): void {
    this._router.navigate(['/']);
  }

  getModelTooltip(): string {
    if (this.model) {
      return this._translatePipe.transform('DETAILS-FOOTER.MODEL-BTN-SELECTION-TOOLTIP') + this.model;
    }

    return this._translatePipe.transform('SETTINGS.NO-CHOSEN-MODEL');
  }

  browseModel(): void {
    this._electronService.remote.dialog
      .showOpenDialog({title: this._translatePipe.transform('SETTINGS.MODEL-SELECTION'), properties: ['openFile']})
      .then(
        modelPath => {
          if (modelPath === undefined){
            return;
          }

          if (modelPath.filePaths[0]) {
            this.model = modelPath.filePaths[0];
          }
        }
      );
  }

  selectThreshold(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { threshold: this.threshold };

    const sub = this._matDialog.open(ThresholdDialogComponent, dialogConfig).afterClosed().subscribe(threshold => {
      if (threshold) {
        this.threshold = threshold;
      }

      sub.unsubscribe();
    });
  }

  async analyze(): Promise<void> {
    if (!this.settings.dicomDir) {
      if (!(await this._openPropertyDialog(
        this._translatePipe.transform('SETTINGS.NO-DICOM-DIR'),
        this._translatePipe.transform('SETTINGS.SELECT-DIR')))) {
        return;
      }

      await this._openSelectionDialog(
        this._translatePipe.transform('SETTINGS.DICOM-DIR-SELECTION'), 'dicomDir', 'openDirectory');
    }

    if (!this.model) {
      if (!(await this._openPropertyDialog(
        this._translatePipe.transform('SETTINGS.NO-CHOSEN-MODEL'),
        this._translatePipe.transform('SETTINGS.SELECT-MODEL')))) {
        return;
      }

      await this._openSelectionDialog(
        this._translatePipe.transform('SETTINGS.MODEL-SELECTION'), 'modelFile', 'openFile');
    }

    if (!this.settings.serviceDir) {
      if (!(await this._openPropertyDialog(
        this._translatePipe.transform('SETTINGS.NO-SERVICE-DIR'),
        this._translatePipe.transform('SETTINGS.SELECT-DIR')))) {
        return;
      }

      await this._openSelectionDialog(
        this._translatePipe.transform('SETTINGS.SERVICE-DIR-SELECTION'), 'serviceDir', 'openDirectory');
    }

    if (!this.settings.resultsDir) {
      if (!(await this._openPropertyDialog(
        this._translatePipe.transform('SETTINGS.NO-RESULTS-DIR'),
        this._translatePipe.transform('SETTINGS.SELECT-DIR')))) {
        return;
      }

      await this._openSelectionDialog(
        this._translatePipe.transform('SETTINGS.RESULTS-DIR-SELECTION'), 'resultsDir', 'openDirectory');
    }

    this._analyzeService.analyzeFromDetails(this.detailsFolder, this.model, this.threshold);
  }

  private _openPropertyDialog(message: string, buttonName: string): Promise<boolean> {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { message, buttonName };

    return this._matDialog.open(PropertyDialogComponent, dialogConfig).afterClosed().toPromise();
  }

  private _openSelectionDialog(
    label: string,
    propertyName: keyof Settings,
    dialogType: 'openDirectory' | 'openFile'): Promise<void> {
    return this._electronService.remote.dialog
      .showOpenDialog({title: label, properties: [dialogType]})
      .then(
        folderPath => {
          if (folderPath === undefined){
            return;
          }

          if (folderPath.filePaths[0]) {
            if ('modelFile' === propertyName) {
              this.model = folderPath.filePaths[0];
            } else {
              // @ts-ignore
              this.settings[propertyName] = folderPath.filePaths[0];
            }
          }
        }
      );
  }

  private _resolveEndScan() {
    if (this._scanStatus.result.toUpperCase() !== 'OK') {
      this._openResultDialog(
        this._translatePipe.transform('DETAILS-FOOTER.ANALYSIS-FAILED') + this._scanStatus.message);
      return;
    }

    if (this._resultsService.results) {
      const lastResultIndex = this._resultsService.results.value.length - 1;

      if (this._resultsService.results.value[lastResultIndex].main.positive === 0) {
        this._openResultDialog(this._translatePipe.transform('DETAILS-FOOTER.ANALYSIS-NO-RESULTS'));
        return;
      }

      this._openResultDialog(this._translatePipe.transform('DETAILS-FOOTER.ANALYSIS-SUCCESS')).then(confirm => {
        if (confirm) {
          this._resultsService.showDetails(lastResultIndex);
        }
      });
    }
  }

  private _openResultDialog(message: string): Promise<boolean> {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { message };

    return this._matDialog.open(ScanResultDialogComponent, dialogConfig).afterClosed().toPromise();
  }
}
