import { Component } from '@angular/core';
import { SystemSettingsService } from './shared/service/system-settings.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private _systemSettingsService: SystemSettingsService) { }
}
