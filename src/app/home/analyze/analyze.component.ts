import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { ElectronService } from 'ngx-electron';
import { Subscription } from 'rxjs';

import { Settings } from '../../shared/types/settings.type';
import { AnalyzeService } from '../../shared/service/analyze.service';
import { SettingsService } from '../../shared/service/settings.service';
import { PropertyDialogComponent } from './property-dialog/property-dialog.component';
import { ResultsService } from '../../shared/service/results.service';
import { ScanStatus } from '../../shared/types/scan-status.type';
import { ScanStatusService } from '../../shared/service/scan-status.service';
import { TranslatePipe } from '@ngx-translate/core';

@Component({
  selector: 'app-analyze',
  templateUrl: './analyze.component.html'
})
export class AnalyzeComponent implements OnDestroy, AfterViewInit {
  private _settingsSub: Subscription | null = null;
  private _scanStatusSub: Subscription | null = null;
  private _analysisInProgressSub: Subscription | null = null;
  analysisInProgress = false;
  settings: Settings;
  scanStatus: ScanStatus = {
    result: 'OK',
    message: ''
  }

  constructor(
    private _analyzeService: AnalyzeService,
    private _electronService: ElectronService,
    private _matDialog: MatDialog,
    private _resultsService: ResultsService,
    private _scanStatusService: ScanStatusService,
    private _settingsService: SettingsService,
    private _translatePipe: TranslatePipe,
    private _changeDetectorRef: ChangeDetectorRef) {
    this.settings = this._settingsService.defaultSettings;

    this._settingsSub = this._settingsService.settingsSubject.subscribe(settings => {
      this.settings = settings;
    });

    this._scanStatusSub = this._scanStatusService.lastScanStatus.subscribe(scanResult => {
      this.scanStatus = scanResult;
    });
  }

  ngAfterViewInit(): void {
    this._analysisInProgressSub = this._analyzeService.analysisInProgressSubj.subscribe(isInProgress => {
      this.analysisInProgress = isInProgress;
      this._resultsService.loadResults();
      this._changeDetectorRef.detectChanges();
    });
  }

  ngOnDestroy(): void {
    if (this._settingsSub) {
      this._settingsSub.unsubscribe();
    }

    if (this._analysisInProgressSub) {
      this._analysisInProgressSub.unsubscribe();
    }

    if (this._scanStatusSub) {
      this._scanStatusSub.unsubscribe();
    }
  }

  async analyze(): Promise<void> {
    let updateSettings = false;

    if (!this.settings.dicomDir) {
      updateSettings = true;
      if (!(await this._openPropertyDialog(
          this._translatePipe.transform('SETTINGS.NO-DICOM-DIR'),
          this._translatePipe.transform('SETTINGS.SELECT-DIR')))) {
        return;
      }

      await this._openSelectionDialog(
        this._translatePipe.transform('SETTINGS.DICOM-DIR-SELECTION'), 'dicomDir', 'openDirectory');
    }

    if (!this.settings.modelFile) {
      if (!(await this._openPropertyDialog(
        this._translatePipe.transform('SETTINGS.NO-CHOSEN-MODEL'),
        this._translatePipe.transform('SETTINGS.SELECT-MODEL')))) {
        if (updateSettings) {
          this._settingsService.setSettings(this.settings);
        }
        return;
      }
      updateSettings = true;

      await this._openSelectionDialog(
        this._translatePipe.transform('SETTINGS.MODEL-SELECTION'), 'modelFile', 'openFile');
    }

    if (!this.settings.serviceDir) {
      if (!(await this._openPropertyDialog(
        this._translatePipe.transform('SETTINGS.NO-SERVICE-DIR'),
        this._translatePipe.transform('SETTINGS.SELECT-DIR')))) {
        if (updateSettings) {
          this._settingsService.setSettings(this.settings);
        }
        return;
      }
      updateSettings = true;

      await this._openSelectionDialog(
        this._translatePipe.transform('SETTINGS.SERVICE-DIR-SELECTION'), 'serviceDir', 'openDirectory');
    }

    if (!this.settings.resultsDir) {
      if (!(await this._openPropertyDialog(
        this._translatePipe.transform('SETTINGS.NO-RESULTS-DIR'),
        this._translatePipe.transform('SETTINGS.SELECT-DIR')))) {
        return;
      }
      updateSettings = true;

      await this._openSelectionDialog(
        this._translatePipe.transform('SETTINGS.RESULTS-DIR-SELECTION'), 'resultsDir', 'openDirectory');
    }

    if (updateSettings) {
      this._settingsService.setSettings(this.settings);
    }

    this._analyzeService.analyze();
  }

  async openDicomSelect(): Promise<void> {
    await this._openSelectionDialog(
      this._translatePipe.transform('SETTINGS.DICOM-DIR-SELECTION'),
      'dicomDir',
      'openDirectory',
      this.settings.dicomDir);

    this._settingsService.setSettings(this.settings);
  }

  private _openPropertyDialog(message: string, buttonName: string): Promise<boolean> {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { message, buttonName };

    return this._matDialog.open(PropertyDialogComponent, dialogConfig).afterClosed().toPromise();
  }

  private _openSelectionDialog(
      label: string,
      propertyName: keyof Settings,
      dialogType: 'openDirectory' | 'openFile',
      defaultPath?: string): Promise<void> {
    const options: any = {title: label, properties: [dialogType]};

    if (defaultPath) {
      options.defaultPath = defaultPath;
    }

    return this._electronService.remote.dialog
      .showOpenDialog(options)
      .then(
        folderPath => {
          if (folderPath === undefined){
            return;
          }

          if (folderPath.filePaths[0]) {
            // @ts-ignore
            this.settings[propertyName] = folderPath.filePaths[0];
          }
        }
      );
  }
}
