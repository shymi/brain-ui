import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-property-dialog',
  templateUrl: './property-dialog.component.html'
})
export class PropertyDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<PropertyDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { message: string, buttonName: string }) { }

  close(showSelectionDialog: boolean): void {
    this.dialogRef.close(showSelectionDialog);
  }
}
