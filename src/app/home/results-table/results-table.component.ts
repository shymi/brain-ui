import { AfterViewInit, ChangeDetectorRef, Component, NgZone, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';

import { ScanResult } from '../../shared/types/scan-result.type';
import { ResultsService } from '../../shared/service/results.service';

@Component({
  selector: 'app-results-table',
  templateUrl: './results-table.component.html'
})
export class ResultsTableComponent implements OnDestroy, AfterViewInit {
  private _resultsSub: Subscription | null = null;
  results: ScanResult[] = [];

  constructor(
    private _changeDetectionRef: ChangeDetectorRef,
    private _resultsService: ResultsService,
    private _ngZone: NgZone) {}

  ngAfterViewInit(): void {
    this._resultsSub = this._resultsService.results.subscribe(results => {
      this.results = results;
      this._ngZone.run(() => { this._changeDetectionRef.detectChanges(); });
    });
  }

  ngOnDestroy(): void {
    if (this._resultsSub) {
      this._resultsSub.unsubscribe();
    }
  }

  getRowClass(result: ScanResult): string {
    if (result.main.positive && result.main.positive > 0) {
      return 'table-danger';
    }

    return 'table-success';
  }

  showDetails(index: number) {
    this._resultsService.showDetails(index);
  }
}
