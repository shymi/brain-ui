import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Settings } from '../shared/types/settings.type';

import { ResultsService } from '../shared/service/results.service';
import { SettingsService } from '../shared/service/settings.service';
import { ScanResult } from '../shared/types/scan-result.type';
import { ElectronService } from 'ngx-electron';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnDestroy, AfterViewInit {
  private _settingsSub: Subscription | null = null;
  private _resultsSub: Subscription | null = null;
  settings: Settings;
  results: ScanResult[] = [];

  constructor(
    private _settingsService: SettingsService,
    private _electronService: ElectronService,
    private _resultsService: ResultsService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _translateService: TranslateService) {
    this.settings = this._settingsService.defaultSettings;
    this._settingsSub = this._settingsService.settingsSubject.subscribe(settings => {
      this.settings = settings;
    });
    this._electronService.ipcRenderer.on('python-close', () => {
      this._changeDetectorRef.detectChanges();
    });
  }

  ngAfterViewInit(): void {
    this._resultsSub = this._resultsService.results.subscribe(results => {
      this.results = results;
      this._changeDetectorRef.markForCheck();
    });
  }

  ngOnDestroy(): void {
    if (this._settingsSub) {
      this._settingsSub.unsubscribe();
    }

    if (this._resultsSub) {
      this._resultsSub.unsubscribe();
    }
  }
}
