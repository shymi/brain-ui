import { spawn } from 'child_process';
import * as fs from 'fs';
import * as electron from 'electron';
import * as path from 'path';

import { AnalyzeSettings } from '../../src/app/shared/types/analyze-settings.type';
import { SettingsUtils } from './settings.utils';

export class AnalyzeUtils {
  static startAnalyze(settings: AnalyzeSettings, serviceDir: string): void {
    const scriptPath = path.join(serviceDir, settings.scriptName)
    const python = spawn(
      settings.executor,
      [
        scriptPath,
        settings.settingsParamName,
        SettingsUtils.getSettingsFolder()
      ]);

    python.stdout.on('data', data => {
      const stringData = data.toString();
      electron.webContents.getAllWebContents().forEach(wc => wc.send('python-data', stringData));
    });

    python.stderr.on('data', data => {
      const stringData = data.toString();
      electron.webContents.getAllWebContents().forEach(wc => wc.send('python-error', stringData));
    });

    python.on('close', (code) => {
      electron.webContents.getAllWebContents().forEach(wc => wc.send('python-close', code));
    });
  }

  static startDetailsAnalyze(
    settings: AnalyzeSettings,
    serviceDir: string,
    folder: string,
    model: string,
    threshold: number): void {
    const scriptPath = path.join(serviceDir, settings.scriptName)
    const python = spawn(
      settings.executor,
      [
        scriptPath,
        settings.settingsParamName,
        SettingsUtils.getSettingsFolder(),
        settings.folderParamName,
        folder,
        settings.modelParamName,
        model,
        settings.thresholdParamName,
        threshold.toString()
      ]);

    python.stdout.on('data', data => {
      const stringData = data.toString();
      electron.webContents.getAllWebContents().forEach(wc => wc.send('python-data', stringData));
    });

    python.stdout.on('error', data => {
      const stringData = data.toString();
      electron.webContents.getAllWebContents().forEach(wc => wc.send('python-error', stringData));
    });

    python.on('close', (code) => {
      electron.webContents.getAllWebContents().forEach(wc => wc.send('python-close', code));
    });
  }

  static loadSettings(defaultSettings: AnalyzeSettings): AnalyzeSettings {
    const settingsPath = AnalyzeUtils.getSettingsLocation();
    let settings;

    if (fs.existsSync(settingsPath)) {
      settings = JSON.parse(fs.readFileSync(settingsPath).toString());
    } else {
      settings = defaultSettings;
      fs.writeFileSync(settingsPath, JSON.stringify(defaultSettings));
    }

    return settings;
  }

  static saveSettings(settings: AnalyzeSettings): void {
    fs.writeFileSync(AnalyzeUtils.getSettingsLocation(), JSON.stringify(settings));
  }

  static getSettingsLocation(): string {
    const userDataPath = electron.app.getPath('userData');

    return path.join(userDataPath, 'brainUIAnalyze.json');
  }
}
