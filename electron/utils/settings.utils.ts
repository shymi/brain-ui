import * as path from 'path';
import * as fs from 'fs';

import * as electron from 'electron';

import { Settings } from '../../src/app/shared/types/settings.type';

export class SettingsUtils {
  static loadSettings(defaultSettings: Settings): Settings {
    const settingsPath = SettingsUtils.getSettingsLocation();
    let settings;

    if (fs.existsSync(settingsPath)) {
      settings = JSON.parse(fs.readFileSync(settingsPath).toString());
    } else {
      settings = defaultSettings;
      fs.writeFileSync(settingsPath, JSON.stringify(defaultSettings));
    }

    return settings;
  }

  static saveSettings(settings: Settings): void {
    fs.writeFileSync(SettingsUtils.getSettingsLocation(), JSON.stringify(settings));
  }

  static getSettingsLocation(): string {
    const userDataPath =  electron.app.getPath('userData');

    return path.join(userDataPath, 'brainUI.json');
  }

  static getSettingsFolder(): string {
    return electron.app.getPath('userData');
  }
}
