import * as fs from 'fs';
import * as path from 'path';
import { ScanResult } from '../../src/app/shared/types/scan-result.type';

export class ResultsUtils {
  static MAIN_JSON_NAME = 'main.json';
  static POSITIVE_FOLDER_NAME = 'positive';
  static POSITIVE_FILE_INFO_TYPE = '.json';

  static loadMainResults(resultsPath: string): ScanResult[] {
    const results: ScanResult[] = [];

    // check if results folder exists
    if (fs.existsSync(resultsPath)) {
      const folders
        = fs.readdirSync(resultsPath, { withFileTypes: true })
            .filter(item => item.isDirectory())
            .map(item => item.name);
      if (folders && folders.length > 0) {
        for (const folder of folders) {
          const folderPath = path.join(resultsPath, folder);
          const mainJsonPath = path.join(folderPath, ResultsUtils.MAIN_JSON_NAME);

          // check if folder contains main json
          if (fs.existsSync(mainJsonPath)) {
            const item: ScanResult = {main: {}, path: folderPath};
            results.push(ResultsUtils.loadResults(item, mainJsonPath));
          }
        }
      }
    }

    return results;
  }

  static getPathToImage(pathToResult: string, imageName: string): string {
    return path.join(pathToResult, ResultsUtils.POSITIVE_FOLDER_NAME, imageName);
  }

  private static loadResults(scanResult: ScanResult, mainJsonPath: string): ScanResult {
    // load root main.json
    scanResult.main = JSON.parse(fs.readFileSync(mainJsonPath).toString());

    if (scanResult.main.positive && scanResult.main.positive > 0) {
      const positiveFolderPath = path.join(scanResult.path, ResultsUtils.POSITIVE_FOLDER_NAME);
      // check if folder contains main json
      if (fs.existsSync(positiveFolderPath)) {
        const positiveResults
          = fs.readdirSync(positiveFolderPath)
          .filter(item => item.indexOf(ResultsUtils.POSITIVE_FILE_INFO_TYPE) >= 0)
          .map(item => item);

        scanResult.positive = [];
        for (const item of positiveResults) {
          const resultJsonPath = path.join(positiveFolderPath, item);
          if (fs.existsSync(resultJsonPath)) {
            scanResult.positive.push(JSON.parse(fs.readFileSync(resultJsonPath).toString()));
          }
        }
      }
    }

    return scanResult;
  }
}
