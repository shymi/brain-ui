import * as path from 'path';
import * as fs from 'fs';
import { ScanStatus } from '../../src/app/shared/types/scan-status.type';

export class ScanStatusUtils {
  private static LAST_RESULT_FILE_NAME = 'last_result.json';
  private static LAST_RESULT_DIR_NAME = 'json';
  private static CANNOT_FIND_SCAN_STATUS: ScanStatus = {
    result: 'Failure',
    message: 'Could not find scan status file'
  }

  static getLatestScanStatus(resultsDir: string): ScanStatus {
    const lastResultPath = path.join(resultsDir, ScanStatusUtils.LAST_RESULT_DIR_NAME, ScanStatusUtils.LAST_RESULT_FILE_NAME);
    let scanStatus: ScanStatus;

    if (fs.existsSync(lastResultPath)) {
      scanStatus = JSON.parse(fs.readFileSync(lastResultPath).toString());
    } else {
      scanStatus = ScanStatusUtils.CANNOT_FIND_SCAN_STATUS;
    }

    return scanStatus;
  }
}
