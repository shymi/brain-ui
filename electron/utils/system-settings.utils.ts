import * as path from 'path';
import * as fs from 'fs';

import * as electron from 'electron';

import { SystemSettings } from '../../src/app/shared/types/system-settings.type';
import { WindowMenuInitialize } from '../window-menu-initialize';

export class SystemSettingsUtils {
  static loadSystemSettings(defaultSettings: SystemSettings): SystemSettings {
    const settingsPath = SystemSettingsUtils.getSettingsLocation();
    let settings;

    if (fs.existsSync(settingsPath)) {
      settings = JSON.parse(fs.readFileSync(settingsPath).toString());
    } else {
      settings = defaultSettings;
      fs.writeFileSync(settingsPath, JSON.stringify(defaultSettings));
    }

    WindowMenuInitialize.createMenu(settings.language);

    return settings;
  }

  static saveSystemSettings(settings: SystemSettings): void {
    fs.writeFileSync(SystemSettingsUtils.getSettingsLocation(), JSON.stringify(settings));
    WindowMenuInitialize.createMenu(settings.language);
  }

  static getSettingsLocation(): string {
    const userDataPath =  electron.app.getPath('userData');

    return path.join(userDataPath, 'system.json');
  }
}
