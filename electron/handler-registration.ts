import { ipcMain } from 'electron';

import { AnalyzeUtils } from './utils/analyze.utils';
import { ResultsUtils } from './utils/results.utils';
import { SettingsUtils } from './utils/settings.utils';
import { ScanStatusUtils } from './utils/scan-status.utils';
import { SystemSettingsUtils } from './utils/system-settings.utils';

export class HandlerRegistration {
  static registerHandlers(): void {
    // settings handlers
    ipcMain.handle('load-settings', (event, settings) => {
      return SettingsUtils.loadSettings(settings);
    });
    ipcMain.handle('save-settings', (event, settings) => {
      SettingsUtils.saveSettings(settings);
    });
    ipcMain.handle('settings-get-location', (event) => {
      return SettingsUtils.getSettingsFolder();
    });

    // results handlers
    ipcMain.handle('load-results', (event, resultsFolder) => {
      return ResultsUtils.loadMainResults(resultsFolder);
    });
    ipcMain.handle('build-png-path-result', (event, pathToResults, imageName) => {
      return ResultsUtils.getPathToImage(pathToResults, imageName);
    });

    // analyze handlers
    ipcMain.handle('analyze-start', (event, settings, serviceDir) => {
      return AnalyzeUtils.startAnalyze(settings, serviceDir);
    });
    ipcMain.handle('analyze-details-start', (event, settings, serviceDir, folder, model, threshold) => {
      return AnalyzeUtils.startDetailsAnalyze(settings, serviceDir, folder, model, threshold);
    });
    ipcMain.handle('analyze-load-settings', (event, defaultSettings) => {
      return AnalyzeUtils.loadSettings(defaultSettings);
    });
    ipcMain.handle('analyze-save-settings', (event, settings) => {
      return AnalyzeUtils.saveSettings(settings);
    });

    // scan status handlers
    ipcMain.handle('get-latest-scan-status', (event, resultDir) => {
      return ScanStatusUtils.getLatestScanStatus(resultDir);
    });

    // system settings handlers
    ipcMain.handle('load-system-settings', (event, settings) => {
      return SystemSettingsUtils.loadSystemSettings(settings);
    });
    ipcMain.handle('save-system-settings', (event, settings) => {
      SystemSettingsUtils.saveSystemSettings(settings);
    });
  }
}

