import { Menu } from 'electron';

export class WindowMenuInitialize {
  private static _menuItems = {
    "bg": {
      label: 'Инструменти',
      submenu: [
        {
          label: 'Dev Tools',
          accelerator: 'F11',
          role: 'toggleDevTools'
        }
      ]
    },
    "en": {
      label: 'Instruments',
      submenu: [
        {
          label: 'Dev Tools',
          accelerator: 'F11',
          role: 'toggleDevTools'
        }
      ]
    }
  }

  static createMenu(lang: string): void {
    Menu.setApplicationMenu(Menu.buildFromTemplate([
        // @ts-ignore
        WindowMenuInitialize._menuItems[lang]
      ])
    );
  }
}

