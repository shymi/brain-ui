# BrainUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.2.

## Development mode

Run `npm start` for a dev mode. This will start the app in an electron window and will open dev tools.

## Build

Running `npm run electron:build` will currently generate a single file as well as an exploded version of the app.

## Linux build from Windows
 - Install Docker
 - Download the electronuserland/builder Docker image with the following console command:

`docker pull electronuserland/builder`

 - From the Electron project's root folder (such as C:\MyApp), type the following command-line command to run the container and map the Electron project's root folder to the /project virtual path:

`docker run --rm -ti -v C:\MyApp\:/project -w /project electronuserland/builder`

 - From inside the container globally install the electron-builder package and build the Linux redistributable package:

`yarn global add electron-builder`

`npm run electron:build`
